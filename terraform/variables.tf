variable "logging_chart_version" { 
    type = string
    default = "3.9.5"
}

variable "helm_logging_namespace" { 
    type = string
    default = "logging"
}

variable "cluster_name" {
   type = string
   default = "gitlab-presentation"
}