module "eks" {
  source = "terraform-aws-modules/eks/aws"
  version = "17.13.0"
  
  create_eks = true
  cluster_name = var.cluster_name
  cluster_version = "1.20"

  vpc_id  = module.vpc.vpc_id
  subnets = module.vpc.private_subnets

  kubeconfig_output_path = "./kubeconfig/"
  enable_irsa = true
  cluster_enabled_log_types = ["api", "audit", "scheduler", "controllerManager", "authenticator"]

  node_groups_defaults = {
    ami_type  = "AL2_x86_64"
    disk_size = 10
  }

    node_groups = {
    development = {
      desired_capacity = 2
      max_capacity     = 5
      min_capacity     = 2
      instance_types = ["t3a.medium"]
      capacity_type  = "SPOT"
     }
  }  

  map_users = var.map_users
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name                 = "${var.cluster_name}-vpc"
  cidr                 = "172.16.0.0/16"
  azs                  = var.azs
  private_subnets      = ["172.16.10.0/24", "172.16.20.0/24", "172.16.30.0/24"]
  public_subnets       = ["172.16.60.0/24", "172.16.70.0/24", "172.16.80.0/24"]
  
  enable_nat_gateway   = false
  single_nat_gateway   = false
  
  enable_dns_hostnames = true
  public_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb"              = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"     = "1"
  }

  tags = {
    "gitlab-k8s-presentation" = "true"
  }
}