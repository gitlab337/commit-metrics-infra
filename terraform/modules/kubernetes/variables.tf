variable "azs" { 
    type = list(string)
    description = "(required) list of avaiability zones"
}

variable "cluster_name" { 
    type = string
    description = "(required) set eks cluster name"
}

variable "map_users" {
  type = list(any)
  description = "(required) set current users to access eks cluster"
}