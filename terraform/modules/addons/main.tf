module "banzai_logging" {
  source = "./banzai-logging"

  helm_logging_namespace = var.helm_logging_namespace
  logging_chart_version  = var.logging_chart_version
}

module "cluster_autoscaler" {
  source = "./cluster-autoscaler"

  cluster_name            = var.cluster_name
  cluster_oidc_issuer_url = var.cluster_oidc_issuer_url

  depends_on = [
    module.prometheus_stack
  ]
}

module "ingress_nginx" {
  source = "./ingress-nginx"

  depends_on = [
    module.prometheus_stack
  ]
}

module "metrics_server" {
  source = "./metrics-server"

  metrics_server_namespace = var.metrics_server_namespace
  create_namespace         = var.create_metrics_server_namespace
}

module "prometheus_stack" {
  source = "./prometheus"

  prometheus_stack_namespace = var.prometheus_stack_namespace
  create_namespace           = var.create_prometheus_namespace

  depends_on = [
    module.metrics_server
  ]
}