locals {
  helm_chart_values = <<VALUES
createCustomResource: false
VALUES
}

resource "helm_release" "logging_operator" {
  name              = "logging-operator"
  chart             = "logging-operator"
  repository        = "https://kubernetes-charts.banzaicloud.com"
  namespace         = var.helm_logging_namespace
  version           = var.logging_chart_version
  dependency_update = true
  create_namespace  = true
  timeout           = 1200

  values = [local.helm_chart_values]
}

