variable "helm_logging_namespace" {
  type        = string
  description = "(required) describe logging cluster namespace"
}

variable "logging_chart_version" {
  type        = string
  description = "(required) describe logging operator version"
}