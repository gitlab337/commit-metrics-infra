resource "helm_release" "ingress_nginx" {
  name              = "ingress-nginx"
  namespace         = "kube-system"
  chart             = "ingress-nginx"
  version           = "4.0.6"
  repository        = "https://kubernetes.github.io/ingress-nginx"
  create_namespace  = false
  dependency_update = true

  timeout = 1200
  values  = [file("${path.module}/values.yaml")]
}