variable "prometheus_stack_namespace" {
  type        = string
  description = "(optional) namepsace to execute prometheus-stack"
}

variable "create_namespace" {
  type        = bool
  description = "(optional) flag to allow namespace creation"
}