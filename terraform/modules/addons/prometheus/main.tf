locals {
  helm_prometheus_values = <<VALUES
replicas: 2
podDisruptionBudget:
  enabled: true
  minAvaiable: 1 
VALUES
}

resource "helm_release" "prometheus_stack" {
  name              = "prometheus"
  repository        = "https://prometheus-community.github.io/helm-charts"
  chart             = "kube-prometheus-stack"
  version           = "19.0.3"
  create_namespace  = var.create_namespace
  namespace         = var.prometheus_stack_namespace
  timeout           = 1600
  dependency_update = true

  values = [local.helm_prometheus_values]
}