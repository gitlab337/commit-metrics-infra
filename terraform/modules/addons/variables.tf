variable "helm_logging_namespace" {
  type        = string
  description = "(required) describe logging cluster namespace"
}

variable "logging_chart_version" {
  type        = string
  description = "(required) describe logging operator version"
}

variable "cluster_name" {
  type        = string
  description = "(required) eks cluster name"
}

variable "cluster_oidc_issuer_url" {
  type        = string
  description = "(required) eks cluster oidc url"
}

variable "metrics_server_namespace" {
  type        = string
  description = "(optional) namespace to crate metrics-server"
  default     = "kube-system"
}

variable "create_metrics_server_namespace" {
  type        = bool
  description = "(optional) flag to allow a namespace creation"
  default     = false
}

variable "prometheus_stack_namespace" {
  type        = string
  description = "(optional) namepsace to execute prometheus-stack"
  default     = "monitoring"
}

variable "create_prometheus_namespace" {
  type        = bool
  description = "(optional) flag to allow namespace creation"
  default     = true
}