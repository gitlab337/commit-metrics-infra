locals {
  helm_chart_values = <<VALUES
args:
  - --logtostderr
  - --metric-resolution=15s
replicas: 2
podDisruptionBudget:
  enabled: true
  minAvaiable: 1
VALUES
}

resource "helm_release" "metrics_server" {
  repository        = "https://kubernetes-sigs.github.io/metrics-server"
  name              = "metrics-server"
  chart             = "metrics-server"
  version           = "3.5.0"
  create_namespace  = var.create_namespace
  namespace         = var.metrics_server_namespace
  dependency_update = true
  timeout           = 1200

  values = [local.helm_chart_values]
}