variable "metrics_server_namespace" {
  type        = string
  description = "(required) namespace to crate metrics-server"
}

variable "create_namespace" {
  type        = bool
  description = "(required) flag to allow a namespace creation"
}