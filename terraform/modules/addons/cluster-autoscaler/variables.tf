variable "cluster_name" {
  type        = string
  description = "(required) eks cluster name"
}

variable "cluster_oidc_issuer_url" {
  type        = string
  description = "(required) eks cluster oidc url"
}