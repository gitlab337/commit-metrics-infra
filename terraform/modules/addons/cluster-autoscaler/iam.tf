locals {
  role_custom_name = "${var.cluster_name}-asg-irsa"
  cluster_oidc_url = replace(var.cluster_oidc_issuer_url, "https://", "")
  service_account  = "${var.cluster_name}-autoscaler-sa"
}

module "cluster_autoscaler_assume_role_oidc" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version = "4.6.0"

  create_role                   = true
  role_name                     = local.role_custom_name
  provider_url                  = local.cluster_oidc_url
  role_policy_arns              = [aws_iam_policy.cluster_autoscaler_policy.arn]
  number_of_role_policy_arns    = 1
  oidc_fully_qualified_subjects = ["system:serviceaccount:kube-system:${local.service_account}"]
}

resource "aws_iam_policy" "cluster_autoscaler_policy" {
  name   = "EKSClusterPolicy-${var.cluster_name}"
  policy = data.aws_iam_policy_document.cluster_autoscaler.json
}

data "aws_iam_policy_document" "cluster_autoscaler" {
  statement {
    sid    = "EKSClusterAutoScalerPolicy"
    effect = "Allow"

    actions = [
      "autoscaling:Describe*",
      "autoscaling:SetDesiredCapacity",
      "autoscaling:TerminatingInstanceInAutoScalingGroup",
      "ec2:DescribeLaunchTemplateVersions",
      "ec2:DescribeInstanceTypes"
    ]

    resources = ["*"]

    condition {
      test     = "StringEquals"
      variable = "autoscaling:ResourceTag/k8s.io/cluster-autoscaler/${var.cluster_name}"
      values   = ["owned"]
    }
    condition {
      test     = "StringEquals"
      variable = "autoscaling:ResourceTag/k8s.io/cluster-autoscaler/enabled"
      values   = ["true"]
    }
  }
}