resource "helm_release" "autoscaler" {
  name       = "cluster-autoscaler"
  repository = "https://kubernetes.github.io/autoscaler"
  chart      = "cluster-autoscaler"
  version    = "9.10.17"
  namespace  = "kube-system"
  timeout    = 1200

  values = [templatefile("${path.module}/values.yaml.tpl", {
    cluster_name    = var.cluster_name
    aws_sa_asg_role = module.cluster_autoscaler_assume_role_oidc.iam_role_arn
    sa_name         = local.service_account
  })]
  depends_on = [module.cluster_autoscaler_assume_role_oidc]
}