locals {
  account_id = data.aws_caller_identity.current.account_id
}

module "kubernetes_cluster" {
  source = "./modules/kubernetes"

  azs = data.aws_availability_zones.available.names
  cluster_name = var.cluster_name
  map_users = [
    {
      userarn  = "arn:aws:iam::${local.account_id}:user/gitlab-k8s-presentation"
      username = "gitlab-k8s-presentation"
      groups   = ["system:masters"]
    },
  ]
}

module "addons" {
  source = "./modules/addons"

  cluster_name            = module.kubernetes_cluster.cluster_id
  cluster_oidc_issuer_url = module.kubernetes_cluster.cluster_oidc_issuer_url
  helm_logging_namespace  = var.helm_logging_namespace
  logging_chart_version   = var.logging_chart_version

  depends_on = [
    module.kubernetes_cluster
  ]
}

resource "null_resource" "wait_for_cluster" {
  provisioner "local-exec" {
    command = "for i in `seq 1 60`; do wget --no-check-certificate -O - -q $ENDPOINT/healthz >/dev/null && exit 0 || true; sleep 5; done; echo TIMEOUT && exit 1"
    interpreter = [
      "/bin/sh", "-c"
    ]
    environment = {
      ENVIRONMENT = module.kubernetes_cluster.cluster_endpoint
    }
  }

  depends_on = [
    module.kubernetes_cluster
  ]
}